package net.cachedcode.chatnotifications.commands;

import net.cachedcode.chatnotifications.ChatNotifications;
import net.cachedcode.chatnotifications.util.Data;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Notify implements CommandExecutor {

    ChatNotifications plugin;
    public Notify(ChatNotifications plugin) {
        this.plugin = plugin;

        plugin.getCommand("Notify").setExecutor(this);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;

            if(cmd.getLabel().equalsIgnoreCase("Notify")) {
                if(p.hasPermission("notifications.mention.use")) {
                    if(args.length == 0) {
                        if (Data.notifyList.contains(p.getUniqueId())) {
                            p.sendMessage("Notifications are now " + ChatColor.RED + "OFF" + ChatColor.WHITE + ".");
                            Data.notifyList.remove(p.getUniqueId());
                            return true;
                        } else {
                            p.sendMessage("Notifications are now " + ChatColor.GREEN + "ON" + ChatColor.WHITE + ".");
                            Data.notifyList.add(p.getUniqueId());
                            return true;
                        }
                    }
                } else {
                    p.sendMessage(ChatColor.RED + "Invalid Permissions!");
                    return false;
                }
            } else {
                p.sendMessage(ChatColor.RED + "Incorrect Arguments! Use " + ChatColor.WHITE + "/notify");
                return false;
            }
        } else {
            sender.sendMessage("Sorry! Command must be used by a player in game.");
            return false;
        }

        return false;
    }
}
