package net.cachedcode.chatnotifications.notifications;

import net.cachedcode.chatnotifications.ChatNotifications;
import net.cachedcode.chatnotifications.util.Data;
import net.cachedcode.chatnotifications.util.NotificationManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Mention implements Listener {

    ChatNotifications plugin;
    NotificationManager notificationManager;
    public Mention(ChatNotifications plugin, NotificationManager notificationManager) {
        this.plugin = plugin;
        this.notificationManager = notificationManager;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        for(Player p : plugin.getServer().getOnlinePlayers()) {
            if(p.hasPermission("notifications.mention.use")) {
                String msgSent = e.getMessage();

                if(msgSent.contains(ChatColor.stripColor("@"))) {
                    String[] msgSplit = msgSent.split(" ");

                    for(String w : msgSplit) {
                        if((w.startsWith(ChatColor.stripColor("@"))) && (w.length() > "@".length() + 1)) {
                            String wordStpd = w.split("@")[1];

                            String[] wByChar = wordStpd.split("");
                            String[] wByPunc = wordStpd.split("[^a-zA-Z0-9_]");

                            String wFinal = "";
                            for(int i = wByPunc[0].length(); i < wByChar.length; i++) {
                                wFinal = wFinal + wByChar[i];
                            }
                            String receiveName = wByPunc[0];

                            Player receivePlayer = Bukkit.getPlayer(receiveName);

                            if(p.getName().equals(receiveName)) {
                                p.sendMessage("Can't Ping Yourself!");
                            }

                            if(receivePlayer != null) {
                                if(Data.notifyList.contains(receivePlayer.getUniqueId())) {
                                    this.notificationManager.send(receivePlayer);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
