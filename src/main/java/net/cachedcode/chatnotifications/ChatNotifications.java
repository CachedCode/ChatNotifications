package net.cachedcode.chatnotifications;

import net.cachedcode.chatnotifications.commands.Notify;
import net.cachedcode.chatnotifications.notifications.Mention;
import net.cachedcode.chatnotifications.util.NotificationManager;
import org.bukkit.plugin.java.JavaPlugin;

public class ChatNotifications extends JavaPlugin {

    public void onEnable() {
        new Notify(this);
        new Mention(this, new NotificationManager(this));
    }

}
