package net.cachedcode.chatnotifications.util;

import net.cachedcode.chatnotifications.ChatNotifications;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class NotificationManager {

    ChatNotifications plugin;
    public NotificationManager(ChatNotifications plugin) {
        this.plugin = plugin;
    }

    public void send(Player player) {
        if(!(Data.notifyList.contains(player.getUniqueId()))) {
            return;
        } else if(player != null) {
            Location playerLoc = player.getLocation();
            player.playSound(playerLoc, Sound.BLOCK_NOTE_PLING, 1.0f, 2f);
        }
    }

}
